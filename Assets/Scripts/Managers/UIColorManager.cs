﻿using UnityEngine;
using UnityEngine.UI;


public class UIColorManager : Manager<UIColorManager>
{
	// Start is called before the first frame update
	[SerializeField]
	private Material headline1Mat = null;
	[SerializeField]
	private Material headline2Mat = null;

	[SerializeField]
	private Image[] imageOutlines = null;

	public void SetHeadlinesColors(Color color1, Color color2)
	{
		headline1Mat.SetColor("_OutlineColor", color1);
		headline2Mat.SetColor("_OutlineColor", color2);
		//headline.outlineColor = color32;
	}

	public void SetImagesColor(Color color)
	{
		foreach (Image image in imageOutlines)
		{
			image.color = color;
		}
	}
}
