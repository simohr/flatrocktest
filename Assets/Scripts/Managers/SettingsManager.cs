﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SettingsManager : Manager<SettingsManager>
{
	public bool Vibrate
	{
		get
		{
			return vibrate;
		}
		set
		{
			vibrate = value;
			PlayerPrefs.SetInt("Vibrate", vibrate ? 1 : 0);
		}
	}

	[SerializeField]
	private GameObject settingsMenu = null;
	private bool vibrate;
	// Start is called before the first frame update
	void Start()
	{
		bool soundActive = PlayerPrefs.GetInt("Sound", 1) == 1 ? true : false;
		SoundSetActive(soundActive);
		Vibrate = PlayerPrefs.GetInt("Vibrate", 1) == 1 ? true : false;
		settingsMenu.GetComponent<SettingsMenu>().InitializeToggles(soundActive, Vibrate);
	}

	public void SoundSetActive(bool active)
	{
		AudioListener.volume = active ? 1 : 0;
		PlayerPrefs.SetInt("Sound", (int)AudioListener.volume);
	}
}
