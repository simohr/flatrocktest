﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LevelManager : Manager<LevelManager>
{
	public Player player;

	public AudioSource soundPlayer;

	public List<Color> colors = new List<Color>()
	{
		Color.red,
		Color.blue,
		Color.green,
		Color.cyan,
		Color.magenta,
	};

	[SerializeField]
	private Color[] additionalColors = null;
	[SerializeField]
	private Sprite[] playerSprites = null;

	public Color playerColor1;
	public Color playerColor2;
	public Color notPlayerColor;

	[SerializeField]
	private GameObject restartMenu = null;
	[SerializeField]
	private GameObject levelUpMenu = null;
	[SerializeField]
	private GameObject experienceBar = null;

	private int level = 1;
	private float experienceNeeded = 0;
	private float currentExperience = 0;

	private bool disableExperienceGathering = false;

	void Start()
	{
		InitializeLevel();
		foreach (var color in additionalColors)
		{
			colors.Add(color);
		}

		LoadPlayerSprite();
		SelectStartingColors();
	}

	public void KillPlayer()
	{
		SpawnerManager.Instance.DisableSpawn();
		player.SpawnDeathParticles();
		player.gameObject.SetActive(false);
		StartCoroutine(EnableRestartMenu());
	}

	public void EnablePlayer()
	{
		player.gameObject.SetActive(true);
	}

	public void DisablePlayer()
	{
		player.gameObject.SetActive(false);
	}

	public void AddExperience(float experience)
	{
		if (disableExperienceGathering)
			return;

		currentExperience += experience;
		if(currentExperience >= experienceNeeded)
		{
			LevelUp();
		}
		else
		{
			experienceBar.GetComponent<ExperienceBar>().UpdateExperienceSlider(currentExperience, experienceNeeded);
		}
	}

	public void InitializeNewLevel()
	{
		SelectStartingColors();
		EnablePlayer();
		SpawnerManager.Instance.EnableSpawn();
	}

	public void SetActiveExperienceBar(bool active)
	{
		experienceBar.SetActive(active);
	}

	public void SetPlayerSprite(Sprite sprite)
	{
		for (int i = 0; i < playerSprites.Length; i++)
		{
			if(playerSprites[i] == sprite)
			{
				PlayerPrefs.SetInt("PlayerSprite", i);
				break;
			}
		}
		player.SetPlayerSprites(sprite);
	}

	private void LevelUp()
	{
		SpawnerManager.Instance.DisableSpawn();
		SetActiveExperienceBar(false);
		StartCoroutine(EnableLevelUpMenu());
		level++;
		experienceNeeded = level * 10 + 10;
		currentExperience = 0;
		experienceBar.GetComponent<ExperienceBar>().SetLevels(level, level + 1);
		experienceBar.GetComponent<ExperienceBar>().UpdateExperienceSlider(currentExperience, experienceNeeded);
		SpawnerManager.Instance.SetDifficulty(level);
		PlayerPrefs.SetInt("Level", level);
	}

	private void InitializeLevel()
	{
		level = PlayerPrefs.GetInt("Level", 1);
		SpawnerManager.Instance.SetDifficulty(level);
		currentExperience = 0;
		experienceNeeded = level * 10 + 10;
		experienceBar.GetComponent<ExperienceBar>().SetLevels(level, level + 1);
	}

	private void LoadPlayerSprite()
	{
		int spriteIndex = PlayerPrefs.GetInt("PlayerSprite", 0);
		player.SetPlayerSprites(playerSprites[spriteIndex]);
	}

	private void SelectStartingColors()
	{
		int randomIndex = Random.Range(0, colors.Count);
		playerColor1 = colors[randomIndex];
		var availablePlayerColors = colors.Where(i => i != playerColor1).ToList();
		randomIndex = Random.Range(0, availablePlayerColors.Count);
		playerColor2 = availablePlayerColors[randomIndex];
		player.SetCirclesColors(playerColor1, playerColor2);
		UIColorManager.Instance.SetHeadlinesColors(playerColor1, playerColor2);
		UIColorManager.Instance.SetImagesColor(playerColor1);

		var availableNonPlayerColors = availablePlayerColors.Where(i => i != playerColor2).ToList();
		randomIndex = Random.Range(0, availableNonPlayerColors.Count);
		notPlayerColor = availableNonPlayerColors[randomIndex];
	}

	private IEnumerator EnableLevelUpMenu()
	{
		disableExperienceGathering = true;
		player.disableInput = true;
		player.PlayWinAnimation();
		yield return new WaitForSeconds(2);
		levelUpMenu.SetActive(true);
		player.disableInput = false;
		disableExperienceGathering = false;
	}

	private IEnumerator EnableRestartMenu()
	{
		yield return new WaitForSeconds(1);
		SetActiveExperienceBar(false);
		restartMenu.GetComponent<RestartMenu>().UpdateTexts(level, (int)((currentExperience / experienceNeeded) * 100));
		currentExperience = 0;
		experienceBar.GetComponent<ExperienceBar>().UpdateExperienceSlider(currentExperience, experienceNeeded);
		restartMenu.SetActive(true);
	}

}
