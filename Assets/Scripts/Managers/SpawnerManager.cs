﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : Manager<SpawnerManager>
{
	private const int reverseDirectionChance = 0;
	public List<SpawnSequence> laserTypes;

	[SerializeField]
	private Transform[] spawnPoints = null;
	private int[,] opposites = { { 0, 3 }, { 1, 4 }, { 2, 5 }, { 6, 7 } };
	private int[,] squares = { { 0, 6, 3, 7 }, { 1, 2, 4, 5 } };

	private Transform[] availableSpawnPoints = null;

	private List<GameObject> instantiatedLasers;

	private int difficultyIndex = 4;

	private float spawnCooldown = 5;
	private float spawnTimer = 0;
	private bool shouldSpawn = false;

	private void Start()
	{
		instantiatedLasers = new List<GameObject>();
	}
	private void Update()
	{
		SpawnLaserHandler();
	}

	public void SetDifficulty(int level)
	{
		difficultyIndex = (level / 5 + 1) * 4;

		if(difficultyIndex < 4)
		{
			difficultyIndex = 4;
		}
		else if(difficultyIndex > laserTypes.Count)
		{
			difficultyIndex = laserTypes.Count;
		}

		SetDifficultyCooldown(level);
	}

	public void EnableSpawn()
	{
		shouldSpawn = true;
	}

	public void DisableSpawn()
	{
		shouldSpawn = false;
		DestroyInstantiatedLasers();
		StopAllCoroutines();
	}

	public void RemoveLaser(GameObject laser)
	{
		instantiatedLasers.Remove(laser);
	}

	private void DestroyInstantiatedLasers()
	{
		foreach (var item in instantiatedLasers)
		{
			Destroy(item);
		}
	}

	private void SetDifficultyCooldown(int level)
	{
		spawnCooldown = 5 - (level / 5 + 0.5f);

		if (spawnCooldown < 3)
		{
			spawnCooldown = 3;
		}
		else if (spawnCooldown > 5)
		{
			spawnCooldown = 5;
		}
	}

	private void SpawnLaserHandler()
	{
		if (!shouldSpawn)
			return;

		if (spawnTimer < Time.time)
		{
			int laserIndex = Random.Range(0, difficultyIndex);
			SpawnLaser(laserTypes[laserIndex]);
			spawnTimer = Time.time + spawnCooldown;
		}
	}

	private void SpawnLaser(SpawnSequence sequenceType)
	{
		SetAvailableSpawnPoints(sequenceType);
		StartCoroutine(SpawnStraightLaserCoroutine(sequenceType));
	}

	private IEnumerator SpawnStraightLaserCoroutine(SpawnSequence sequenceType)
	{
		Transform spawnPoint;
		if (sequenceType.sequenceType == SequenceType.Straight)
		{
			spawnPoint = availableSpawnPoints[Random.Range(0, spawnPoints.Length)];
		}
		else
		{
			spawnPoint = availableSpawnPoints[0];
		}

		Vector3 spawnPosition = spawnPoint.position;

		float randomAngleDeviation = Random.Range(-10f, 10f);

		Vector3 spawnEulerAngles = new Vector3(0, 0, spawnPoint.eulerAngles.z + randomAngleDeviation);

		Color laserColor = Random.Range(0, 2) == 0 ? LevelManager.Instance.playerColor1 : LevelManager.Instance.playerColor2;

		bool reverseDirection = Random.Range(0, 100) < reverseDirectionChance;

		Vector2 positionDeviation = sequenceType.positionDeviation;

		int positiveOrNegative = Random.Range(0, 2) == 0 ? 1 : -1;

		for (int i = 0; i < sequenceType.laserObjects.Length; i++)
		{
			if (sequenceType.sequenceType != SequenceType.Straight)
			{
				int oppositeOrSquare = sequenceType.sequenceType == SequenceType.Square ? 4 : 2;
				int spawnIndex = i < oppositeOrSquare ? i : i % oppositeOrSquare;
				spawnPoint = availableSpawnPoints[spawnIndex];
				spawnPosition = spawnPoint.position;
				spawnEulerAngles = new Vector3(0, 0, spawnPoint.eulerAngles.z + randomAngleDeviation);
			}

			if (sequenceType.laserObjects[i].laserType == LaserType.Shot)
			{
				spawnEulerAngles = new Vector3(0, 0, AngleBetweenTwoPoints(spawnPosition, Vector2.zero));
			}

			if (sequenceType.randomColorForEachLaser)
			{
				laserColor = Random.Range(0, 2) == 0 ? LevelManager.Instance.playerColor1 : LevelManager.Instance.playerColor2;
			}

			if (sequenceType.sequenceType == SequenceType.Opposite)
			{
				laserColor = laserColor == LevelManager.Instance.playerColor1 ? LevelManager.Instance.playerColor2 : LevelManager.Instance.playerColor1;
			}

			Vector3 angleDeviation = new Vector3(0, 0, i * sequenceType.angleDeviation);

			if(sequenceType.positionDeviation != Vector2.zero)
			{
				if(spawnPoint == spawnPoints[6] || spawnPoint == spawnPoints[7])
				{
					positionDeviation.x = 0;
					positionDeviation.y = i * sequenceType.positionDeviation.y * positiveOrNegative;
				}
				else
				{
					positionDeviation.x = i * sequenceType.positionDeviation.x * positiveOrNegative;
					positionDeviation.y = 0;
				}
			}

			GameObject laser = Instantiate(sequenceType.laserObjects[i].laserPrefab, 
				spawnPosition + new Vector3(positionDeviation.x, positionDeviation.y, 0),
				Quaternion.Euler(spawnEulerAngles + angleDeviation));

			if (laser.GetComponent<Laser>() != null)
			{
				laser.GetComponent<Laser>().Initialize(sequenceType.laserObjects[i].speed, laserColor, reverseDirection);
				instantiatedLasers.Add(laser);
			}
			else
			{
				if (sequenceType.laserObjects[i].laserType == LaserType.Separated)
				{
					laserColor = LevelManager.Instance.notPlayerColor;
				}

				foreach (Transform item in laser.transform)
				{
					item.GetComponent<Laser>().Initialize(sequenceType.laserObjects[i].speed, laserColor, reverseDirection);
					instantiatedLasers.Add(item.gameObject);
				}
			}

			yield return new WaitForSeconds(sequenceType.interval);
		}
	}

	private void SetAvailableSpawnPoints(SpawnSequence sequenceType)
	{

		if (sequenceType.sequenceType == SequenceType.Straight)
		{
			availableSpawnPoints = spawnPoints;
		}
		else if (sequenceType.sequenceType == SequenceType.Square)
		{
			int squareIndex = Random.Range(0, squares.GetLength(0));
			int startIndex = Random.Range(0, 4);
			availableSpawnPoints = new Transform[squares.GetLength(1)];
			for (int i = 0; i < availableSpawnPoints.Length; i++)
			{
				availableSpawnPoints[i] = spawnPoints[squares[squareIndex, startIndex]];
				startIndex++;
				if (startIndex >= availableSpawnPoints.Length)
				{
					startIndex = 0;
				}
			}
		}
		else if (sequenceType.sequenceType == SequenceType.Opposite)
		{
			int oppositeIndex = Random.Range(0, opposites.GetLength(0));
			availableSpawnPoints = new Transform[opposites.GetLength(1)];
			for (int i = 0; i < availableSpawnPoints.Length; i++)
			{
				availableSpawnPoints[i] = spawnPoints[opposites[oppositeIndex, i]];
			}
		}
	}

	private float AngleBetweenTwoPoints(Vector2 a, Vector2 b)
	{
		return Mathf.Atan2(b.y - a.y, b.x - a.x) * Mathf.Rad2Deg;
	}
}
