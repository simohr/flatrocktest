﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum LaserType
{
	Whole,
	Shot,
	Separated,
	Crossed
}
[CreateAssetMenu(menuName = "Lasers/Laser")]
public class LaserProperties : ScriptableObject
{
	public GameObject laserPrefab;
	public LaserType laserType;
	public float speed;
}
