﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SequenceType
{
	Straight,
	Square,
	Opposite
}

[CreateAssetMenu(menuName = "Lasers/LaserSequence")]
public class SpawnSequence : ScriptableObject
{
	public SequenceType sequenceType;
	public bool randomColorForEachLaser;
	public LaserProperties[] laserObjects;
	public float interval;
	public float angleDeviation;
	public Vector2 positionDeviation;
}
