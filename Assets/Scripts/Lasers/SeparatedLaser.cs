﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeparatedLaser : Laser
{
	public bool shouldMove = true;

	public override void Initialize(float speed, Color color, bool reverseDirection)
	{
		base.Initialize(speed, color, reverseDirection);
		direction = (Vector3.zero - transform.parent.position).normalized;
	}
	protected override void Move()
	{
		if(shouldMove)
			transform.parent.Translate(direction * speed * Time.deltaTime, Space.World);
	}
}
