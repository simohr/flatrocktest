﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laser : MonoBehaviour
{
	[SerializeField]
	private GameObject deathParticle = null;
	[SerializeField]
	private ParticleSystem particlesFront = null;
	[SerializeField]
	private ParticleSystem particlesBack = null;

	[HideInInspector]
	public Color color;
	[HideInInspector]
	public float speed;

	protected Vector3 direction;
	private bool initialized = false;

	public virtual void Initialize(float speed, Color color, bool reverseDirection)
	{
		direction = (Vector3.zero - transform.position).normalized;
		this.speed = speed;
		this.color = color;
		GetComponent<SpriteRenderer>().color = color;
		var main = particlesFront.main;
		main.startColor = color;
		var main2 = particlesBack.main;
		main2.startColor = color;

		initialized = true;
		Destroy(transform.root.gameObject, 15);
		if(reverseDirection)
		{
			StartCoroutine(ReverseDirection());
		}
	}

	private void Update()
	{
		if (!initialized)
			return;

		Move();
	}

	protected virtual void Move()
	{
		transform.Translate(direction * speed * Time.deltaTime, Space.World);
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.transform.tag == "Player")
		{
			if(color == collision.GetComponent<SpriteRenderer>().color)
			{
				LevelManager.Instance.AddExperience(5);
			}
			else
			{
				LevelManager.Instance.KillPlayer();
			}

			LevelManager.Instance.soundPlayer.Play();
			Destroy(transform.root.gameObject, 1);
			Destroy(gameObject);
		}
	}

	private IEnumerator ReverseDirection()
	{
		yield return new WaitForSeconds(2.3f);

		direction = -direction;
	}

	protected virtual void OnDestroy()
	{
		if(SettingsManager.Instance.Vibrate)
			Handheld.Vibrate();

		SpawnerManager.Instance.RemoveLaser(gameObject);
		GameObject particleEffect = Instantiate(deathParticle, transform.position, transform.rotation);
		var main = particleEffect.GetComponent<ParticleSystem>().main;
		main.startColor = color;
		Destroy(particleEffect, 1);
	}
}
