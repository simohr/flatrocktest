﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	[SerializeField]
	private SpriteRenderer circle1 = null;
	[SerializeField]
	private SpriteRenderer circle2 = null;
	[SerializeField]
	private GameObject deathParticleEffect = null;

	private Vector2 lp;

	private Vector2 mouseDeltaPosition;
	private Vector2 mouseLastFramePosition;

	[HideInInspector]
	public bool isDead = false;
	[HideInInspector]
	public bool disableInput = false;

	// Update is called once per frame
	void Update()
	{
		InputHandler();
	}

	public void SpawnDeathParticles()
	{
		GameObject deathParticle1 = Instantiate(deathParticleEffect, circle1.transform.position, Quaternion.identity);
		GameObject deathParticle2 = Instantiate(deathParticleEffect, circle2.transform.position, Quaternion.identity);
		var main1 = deathParticle1.GetComponent<ParticleSystem>().main; 
		var main2 = deathParticle2.GetComponent<ParticleSystem>().main;
		main1.startColor = circle1.color;
		main2.startColor = circle2.color;
		Destroy(deathParticle1, 1);
		Destroy(deathParticle2, 1);
	}

	public void SetPlayerSprites(Sprite sprite)
	{
		circle1.sprite = sprite;
		circle2.sprite = sprite;
	}

	public Sprite GetPlayerSprite()
	{
		return circle1.sprite;
	}

	public void SetCirclesColors(Color circle1Color, Color circle2Color)
	{
		circle1.color = circle1Color;
		var main = circle1.gameObject.GetComponentInChildren<ParticleSystem>().main;
		main.startColor = circle1Color;

		circle2.color = circle2Color;
		var main2 = circle2.gameObject.GetComponentInChildren<ParticleSystem>().main;
		main2.startColor = circle2Color;
	}

	public void PlayWinAnimation()
	{
		StartCoroutine(WinAnimation());
	}

	private void InputHandler()
	{
		if (disableInput)
			return;
#if UNITY_EDITOR
		MouseInput();
#endif
#if UNITY_ANDROID
		TouchInput();
#endif
	}

	private void MouseInput()
	{
		if (Input.GetMouseButtonDown(0))
		{
			lp = Input.mousePosition;
			mouseDeltaPosition = Input.mousePosition;
			mouseLastFramePosition = Input.mousePosition;
		}

		if (Input.GetMouseButton(0))
		{
			lp = Input.mousePosition;
			mouseDeltaPosition = lp - mouseLastFramePosition;
			mouseLastFramePosition = Input.mousePosition;
			
			float playerRotation = Mathf.Abs(mouseDeltaPosition.x) > Mathf.Abs(mouseDeltaPosition.y) ? mouseDeltaPosition.x : mouseDeltaPosition.y;
			transform.rotation = Quaternion.Euler(transform.eulerAngles + new Vector3(0, 0, playerRotation));
		}
	}

	private void TouchInput()
	{
		foreach (Touch touch in Input.touches)
		{
			if (touch.phase == TouchPhase.Moved)
			{
				float playerRotation = Mathf.Abs(touch.deltaPosition.x) > Mathf.Abs(touch.deltaPosition.y) ? touch.deltaPosition.x : touch.deltaPosition.y;
				transform.rotation = Quaternion.Euler(transform.eulerAngles + new Vector3(0, 0, playerRotation));
			}
		}
	}

	private IEnumerator WinAnimation()
	{
		float timer = 0;
		float speed = 50;

		while(timer < 1)
		{
			transform.Rotate(new Vector3(0, 0, speed * timer));
			timer += Time.deltaTime;
			yield return null;
		}

		while(timer > 0)
		{
			transform.Rotate(new Vector3(0, 0, speed * timer));
			timer -= Time.deltaTime;
			yield return null;
		}
	}
}
