﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ExperienceBar : MonoBehaviour
{
	[SerializeField]
	private TextMeshProUGUI currentLevelText = null;
	[SerializeField]
	private TextMeshProUGUI nextLevelText = null;
	[SerializeField]
	private TextMeshProUGUI percentageText = null;
	[SerializeField]
	private Slider experienceSlider = null;

	public void SetLevels(int currentLevel, int nextLevel)
	{
		currentLevelText.text = currentLevel.ToString();
		nextLevelText.text = nextLevel.ToString();
	}
	public void UpdateExperienceSlider(float currentValue, float maxValue)
	{
		float value = currentValue / maxValue;
		percentageText.text = ((int)(value * 100)).ToString() + "%";
		if(value > 0)
		{
			percentageText.gameObject.SetActive(true);
		}
		else
		{
			percentageText.gameObject.SetActive(false);
		}
		experienceSlider.value = value;
	}
}
