﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUpMenu : MonoBehaviour
{
	[SerializeField]
	private GameObject mainMenu = null;
	public void OnMainMenuClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.EnablePlayer();
		mainMenu.SetActive(true);
		gameObject.SetActive(false);
	}

	public void OnContinueClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.InitializeNewLevel();
		gameObject.SetActive(false);
	}
}
