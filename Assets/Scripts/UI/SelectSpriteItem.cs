﻿using UnityEngine;
using UnityEngine.UI;

public class SelectSpriteItem : MonoBehaviour
{
	[SerializeField]
	private Toggle spriteToggle = null;
	[SerializeField]
	private Image playerSprite = null;

	private void Awake()
	{
		if (playerSprite.sprite == LevelManager.Instance.player.GetPlayerSprite())
		{
			spriteToggle.isOn = true;
			SetColors(LevelManager.Instance.playerColor1);
		}
		else
		{
			spriteToggle.isOn = false;
			SetColors(LevelManager.Instance.playerColor2);
		}
	}

	public void OnSpriteClick()
	{
		if (spriteToggle.isOn)
		{
			SetColors(LevelManager.Instance.playerColor1);
			LevelManager.Instance.SetPlayerSprite(playerSprite.sprite);
		}
		else
		{
			SetColors(LevelManager.Instance.playerColor2);
		}
	}

	private void SetColors(Color color)
	{
		Color backgroundColor = new Color(color.r,
				color.g,
				color.b,
				spriteToggle.GetComponent<Image>().color.a);
		spriteToggle.GetComponent<Image>().color = backgroundColor;
		playerSprite.color = color;
	}
}
