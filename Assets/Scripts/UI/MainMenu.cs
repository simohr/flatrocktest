﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class MainMenu : MonoBehaviour
{
	[SerializeField]
	private GameObject settingsPanel = null;
	[SerializeField]
	private GameObject spriteSelectPanel = null;

	public void OnStartButtonClick()
	{
		this.gameObject.SetActive(false);
		SpawnerManager.Instance.EnableSpawn();
	}

	public void OnSettingsButtonClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(false);
		LevelManager.Instance.DisablePlayer();
		this.gameObject.SetActive(false);
		settingsPanel.SetActive(true);
	}

	public void OnSpriteSelectButtonClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(false);
		LevelManager.Instance.DisablePlayer();
		this.gameObject.SetActive(false);
		spriteSelectPanel.SetActive(true);
	}
}
