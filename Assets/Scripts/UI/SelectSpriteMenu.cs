﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectSpriteMenu : MonoBehaviour
{
	[SerializeField]
	private GameObject mainMenu = null;
	public void OnSpriteClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.EnablePlayer();
		mainMenu.SetActive(true);
		gameObject.SetActive(false);
	}

	public void OnBackClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.EnablePlayer();
		mainMenu.SetActive(true);
		gameObject.SetActive(false);
	}
}
