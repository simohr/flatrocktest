﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class RestartMenu : MonoBehaviour
{
	[SerializeField]
	private GameObject mainMenu = null;
	[SerializeField]
	private TextMeshProUGUI levelText = null;
	[SerializeField]
	private TextMeshProUGUI percentageText = null;
	 
	public void OnMainMenuClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.EnablePlayer();
		mainMenu.SetActive(true);
		gameObject.SetActive(false);
	}

	public void OnRestartClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.EnablePlayer();
		SpawnerManager.Instance.EnableSpawn();
		gameObject.SetActive(false);
	}

	public void UpdateTexts(int level, int percent)
	{
		levelText.text = string.Format("Level {0}", level);
		percentageText.text = string.Format("{0}%", percent);
	}
}
