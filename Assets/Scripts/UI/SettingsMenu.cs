﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
	[SerializeField]
	private GameObject mainMenu = null;
	[SerializeField]
	private Toggle vibrationToggle = null;
	[SerializeField]
	private Toggle soundToggle = null;
	[SerializeField]
	private Sprite vibrationOnImage = null;
	[SerializeField]
	private Sprite vibrationOffImage = null;
	[SerializeField]
	private Sprite soundOnImage = null;
	[SerializeField]
	private Sprite soundOffImage = null;

	public void InitializeToggles(bool soundActive, bool vibrateActive)
	{
		if (vibrateActive)
		{
			vibrationToggle.isOn = true;
			//vibrationToggle.gameObject.GetComponent<Image>().sprite = vibrationOnImage;
		}
		else
		{
			vibrationToggle.isOn = false;
			//vibrationToggle.gameObject.GetComponent<Image>().sprite = vibrationOffImage;
		}

		if(soundActive)
		{
			soundToggle.isOn = true;
		}
		else
		{
			soundToggle.isOn = false;
		}
	}
	public void OnVibrateButtonClick()
	{
		if(vibrationToggle.isOn)
		{
			vibrationToggle.gameObject.GetComponent<Image>().sprite = vibrationOnImage;
			SettingsManager.Instance.Vibrate = true;
		}
		else
		{
			vibrationToggle.gameObject.GetComponent<Image>().sprite = vibrationOffImage;
			SettingsManager.Instance.Vibrate = false;
		}
	}

	public void OnSoundButtonClick()
	{
		if (soundToggle.isOn)
		{
			soundToggle.gameObject.GetComponent<Image>().sprite = soundOnImage;
			SettingsManager.Instance.SoundSetActive(true);
		}
		else
		{
			soundToggle.gameObject.GetComponent<Image>().sprite = soundOffImage;
			SettingsManager.Instance.SoundSetActive(false);
		}
	}

	public void OnBackButtonClick()
	{
		LevelManager.Instance.SetActiveExperienceBar(true);
		LevelManager.Instance.EnablePlayer();
		mainMenu.SetActive(true);
		gameObject.SetActive(false);
	}
}
